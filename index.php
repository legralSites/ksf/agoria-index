<?php

//  ==== configuration du projet ==== //
require("./configLocal/configDB.php");

define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');

define('ISDEV',0);
define('LIB_ROOT','./lib/');
define('PAGESLOCALES_ROOT','./pagesLocales/');

define('LOGOAGORIA_SRC','../agoria-engine/sid/0img/logoagoria.png');

//  === bufferisation ====  //
ob_start();

//  ==== session ==== //
//session_name();session_start();

//  === gestionnaire de librairies ====  //
$lib='gestLib';
include (LIB_ROOT."legral/php/$lib/$lib.php");
/* --- appelle d'une version fixe --- //
include (INTERSITES_ROOT."lib/legral/php/$lib/$lib-v1.0.0.php");
      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
 */

$gestLib->loadLib('agoria-index',__FILE__,'v0.0.1',"portail de selection d'instances et de versions du moteur de jeu Agoria");
	$gestLib->libs['agoria-index']->setErr(LEGRALERR::DEBUG);
$gestLib->setEtat('agoria-index',LEGRAL_LIBETAT::LOADED);
$gestLib->end('agoria-index');

// - gestionnaire de connexion sql - //
$lib='gestPDO';
include(LIB_ROOT."legral/php/$lib/$lib.php");
	//$gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
include("./config/translates/$lib-erreurs.php");

$lib='gestTables';
include(LIB_ROOT."legral/php/gestPDO/$lib.php");
	//$gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
$dbksfV3=new legralPDO('ksfv3');

unset($lib);

//  ==== fonctions ==== //
function convertAccent($text){
	//$ext=htmlentities($text,ENT_NOQUOTES,'UTF-8');$text=htmlspecialchars_decode($text);return $text;
	$carSpec=array('é','è','à','â','ô','î','ç');
	$htmlSpec=array('&eacute;','&egrave;','&agrave;','&acirc;','&ocirc;','&icirc;','&ccedil;');
	return str_replace($carSpec,$htmlSpec,$text);
}
function slasheQuote($text){
	$carSpec=array("'",'"');
	$htmlSpec=array("\'","\"");
	return str_replace($carSpec,$htmlSpec,$text);
}

function ln2br($text){return str_replace("\n",'<br>',$text);}

function formatFile($fName){
$datas='';
if ($f=fopen($fName,'r')){
	$datas=fread($f,filesize($fName));
}
else $datas=-1;
fclose($f);

return convertAccent(ln2br($datas));
}



if(SERVER_RESEAU==='LOCALHOST'){
	define('URLHOST','http://127.0.0.1/git/ksf/');
	define('INDEX_ROOT',DOCUMENT_ROOT.'git/ksf/');
}
else{
	define('URLHOST','https://legral.fr/');
	define('INDEX_ROOT',DOCUMENT_ROOT);
}

//  ==== html ==== //
?><!DOCTYPE html><html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="generator" content="vim">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20150824">
<meta name="date-update-yyyymmdd"   content="20150917">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="never">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">
<title>portail de selection d'instances et de versions du moteur de jeu Agoria</title>

<script src="/locales/scripts.min.js"> </script>

<!-- styles -->
<link rel="stylesheet" href="styles/styles.min.css" media="all" />

<style>
#header{
	background-color:transparent;
}
.grid-4 > *  {
	background: #D6A45E;
	margin-bottom: 10px;
	padding:0 5px;
}
</style>

</head>
<body>
<div id="page">
<!-- header -->
<div id="header">
<div id="headerGauche"></div>
<img style="margin:0 auto" src="<?php echo LOGOAGORIA_SRC;?>" />
</div><!-- header -->

<?php
if($dbksfV3->isConnect()<=0){
	echo '<div class="noteimportant">';
	echo 'Connexion impossible. '.$gestLib->erreurs['gestPDO']->getTexte().'<br>';
	$eTxt=$gestLib->erreurs['gestPDO']->getTexte('e');
	if($eTxt!='')echo "exeption:$eTxt<br>";
	echo '</div>';
	unset ($dbksfv3);
}
?>


<h1>
Portail de selection d'instances et de versions du moteur de jeu Agoria<br>
Acceder &agrave;
   <a target="ksfv3" href="<?php echo URLHOST?>ksfv3/"> l'ancien portail(ksfv3)</a>
 - <a target="ksfv3" href="<?php echo URLHOST?>agoria/stable/"> la derni&egrave;re version stable d'AGORIA</a><br>

admin:
 - sql:  <a target="sql" href="https://phpmyadmin.ovh.net/">prod</a>,<a target="sql" href="">dev</a>
<!--   dépot git: <a target="git" href="https://git.framasoft.org/ksf/ksfv3">framaCode</a>
 - <a target="roadmap" href="https://legral.framapad.org/agoria-roadmap">roadmap</a> -->
</h1>

<h1>
<span id="update" class="switchHead" onclick="var id=document.getElementById('update_datas').style;id.display=id.display=='none'?'block':'none';">Proc&eacute;dure de mise &agrave; jours et r&eacute;troportage</span>
 <span id="checkList" class="switchHead" onclick="var id=document.getElementById('checkList_datas').style;id.display=id.display=='none'?'block':'none';">CheckList</span>
</h1>

<div id="update_datas" class="switchDatas" style="display:none">
	<?php echo formatFile('./update.html');?>

</div>

<div id="checkList_datas" class="switchDatas" style="display:none">
	<?php echo formatFile('./checklist.html');?>
</div>
<br>


<div class="grid-4">
    <div class="flex-item-double"><h1>release</h1></div>
    <div><h1>agoria<br><a target="agoria" href="<?php echo URLHOST?>agoria/">agoria</a></h1></div>
    <div><h1>agoriAlpha<br><a target="agoriAlpha" href="<?php echo URLHOST?>agoriAlpha/">agoriAlpha</a></h1></div>
</div>


<?php
$releases=new gestTable('ksfv3','agoriaIndex_v','id',['SELECT' => '*','ORDERBY'=>'id DESC','clear'=>0]);
if(ISDEV===1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.ln2br($releases->dbTable->sql->getSQL()).'</div>';
//if(ISDEV===1)echo $releases->tableau();


// - sid - //
echo '<div class="grid-4">';
	$v=$releases->get('sid','id');
	echo'<div class="flex-item-double"><h1>'.$v.' - '.$releases->get('sid','date').'</h1>'.ln2br(convertAccent($releases->get('sid','release'))).'</div>';
	echo'<div style="text-align:justify;"><h1><a target="agoria" href="'.URLHOST.'agoria/'.$v.'">agoria/'.$v.'</a></h1>'.ln2br(convertAccent($releases->get('sid','agoria'))).'</div>';
	echo'<div style="text-align:justify;"><h1><a target="agoriAlpha" href="'.URLHOST.'agoriAlpha/'.$v.'">agoriAlpha/'.$v.'</a></h1>'.ln2br(convertAccent($releases->get('sid','agoriAlpha'))).'</div>';
echo'</div>';

// - stable - //
echo '<div class="grid-4">';
	$v=$releases->get('stable','id');
	echo'<div class="flex-item-double"><h1>'.$v.' - '.$releases->get('sid','date').'</h1>'.ln2br(convertAccent($releases->get('stable','release'))).'</div>';
	echo'<div style="text-align:justify;"><h1><a target="agoria" href="'.URLHOST.'agoria/'.$v.'">agoria/'.$v.'</a></h1>'.ln2br(convertAccent($releases->get('stable','agoria'))).'</div>';
	echo'<div style="text-align:justify;"><h1><a target="agoriAlpha" href="'.URLHOST.'agoriAlpha/'.$v.'">agoriAlpha/'.$v.'</a></h1>'.ln2br(convertAccent($releases->get('stable','agoriAlpha'))).'</div>';
echo'</div>';

// - autre release - //
echo '<div class="grid-4">';

foreach ($releases->get() as $v => $release){
	//$v=$release['id'];
	echo'<div class="flex-item-double"><h1>'.$v.' - '.$release['date'].'</h1>'.ln2br(convertAccent($release['release'])).'</div>';

	// - instance: agoria - //
	$iNom='agoria';
	$a=(file_exists(INDEX_ROOT."$iNom/$v"))?"<a target='$iNom' href='".URLHOST."$iNom/$v'>$v</a>":$v;
	echo'<div style="text-align:justify;"><h1>'.$a.'</h1>'.ln2br(convertAccent($release[$iNom])).'</div>';

	// - instance: agoriAlpha - //
	$iNom='agoriAlpha';
	$a=(file_exists(INDEX_ROOT."$iNom/$v"))?"<a target='$iNom' href='".URLHOST."$iNom/$v'>$v</a>":$v;
	echo'<div style="text-align:justify;"><h1>'.$a.'</h1>'.ln2br(convertAccent($release[$iNom])).'</div>';

	
	//	echo'<div style="text-align:justify;"><h1><a target="agoriAlpha" href="'.URLHOST.'agoriAlpha/'.$v.'">agoriAlpha/'.$v.'</a></h1>'.ln2br(convertAccent($release['agoriAlpha'])).'</div>';

}
echo'</div>';

//if(ISDEV===1)include PAGESLOCALES_ROOT.'ksfv3/orgas/dev.php';
//include PAGESLOCALES_ROOT.'footer.php';
?>
